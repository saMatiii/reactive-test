package com.knowledge.test.reactive;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ReactiveApplication {

	private static final Logger log = LoggerFactory.getLogger(ReactiveApplication.class);

	public static void main(String[] args) {
		SpringApplication.run(ReactiveApplication.class, args);

	}
}
