package com.knowledge.test.reactive;

import reactor.core.publisher.Flux;

public interface CommentRepository {

    Flux<Comment> findAll();

}
